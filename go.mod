module gitlab.com/chrisfair/weatherreport

go 1.15

require (
	github.com/araddon/dateparse v0.0.0-20201001162425-8aadafed4dc4 // indirect
	github.com/mmcdole/gofeed v1.1.0 // indirect
	gitlab.com/chrisfair/accuweather v1.0.0
)
