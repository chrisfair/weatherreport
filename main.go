package main

import (
	"flag"
	"fmt"

	"gitlab.com/chrisfair/accuweather"
)

const VERSION = "1.2.0"

func handleCurrentConditions(outputType string, tempUnit string, cityId string) (output string) {

	if outputType == "text" {
		output = accuweather.TextCurrent(tempUnit, cityId)
		return
	}

	if outputType == "icon" {
		output = accuweather.IconCurrent(cityId)
		return

	}
	if outputType == "temp" {
		output = accuweather.TempCurrent(tempUnit, cityId)
		return

	}

	output = accuweather.TextCurrent(tempUnit, cityId)
	return
}

func handleForcastedConditions(outputType string, tempUnit string, cityId string, forcast int) (output string) {
	if outputType == "text" {
		output = accuweather.TextForForcast(tempUnit, cityId, forcast)
		return
	}

	if outputType == "icon" {
		output = accuweather.IconForForcast(cityId, forcast)
		return

	}
	output = accuweather.TextForForcast(tempUnit, cityId, forcast)
	return
}

func handleSelections(outputType string, tempUnit string, cityId string, forcast int) (output string) {

	if forcast < 1 {
		output = handleCurrentConditions(outputType, tempUnit, cityId)
	} else {
		output = handleForcastedConditions(outputType, tempUnit, cityId, forcast)
	}
	return

}
func main() {

	outputText := ""
	_ = outputText
	outputType := flag.String("outputType", "text", "This is the type of output, can be text, icon,  or temp.")
	forcast := flag.Int("forcast", 0, "This is how many days from now to obtain a forcast for.")
	tempUnit := flag.String("tempUnit", "imperial", "This can either be imperial or metric.")
	cityId := flag.String("cityId", "NAM|US|MO|KANSASCITY", "City code.")
	version := flag.Bool("version", false, "This prints the current version of the software")

	flag.Parse()

	if *version == true {
		fmt.Println(VERSION)
		return
	}
	outputText = handleSelections(*outputType, *tempUnit, *cityId, *forcast)
	fmt.Println(outputText)
}
