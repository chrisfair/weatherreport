I simple program to get the weather information from a locale using accuweather then 
to report it to the command line.   It is suitable for use in conky.  I have a function
that retrieves the associated URL from accuwether for the current weather conditions
then reports them to the screen.  I am releasing this GPLv3 for whoever wants to modify
it or use it.


It is pretty easy to use just type weatherReport --help to see the options it currently
defaults to my home town...but you can set it anywhere using the syntax for city location
from www.accuweather.com.    This is utterly dependent on the RSS feed from accuweather
which I hope sticks around.  I was annoyed that I could not find an easy way to get a 
weather section to my conky.  This is my answer to it.

Functionally run the program like thus....
Usage of ./weatherreport:
  -cityId string
    	City code. (default "NAM|US|CO|LOVELAND")
  -forcast int
    	This is how many days from now to obtain a forcast for.
  -outputType string
    	This is the type of output, can be text, icon,  or temp. (default "text")
  -tempUnit string
    	This can either be imperial or metric. (default "imperial")
  -version
    	This prints the current version of the software

My example does not have any city code included but to add it just run the command with the
following.

  weatherReport --cityId="NAM|US|NY"

Will work for New York city.   If you want to change to metric degrees (Celsius) then do the
following....

  weatherReport --cityId="NAM|US|NY" --tempUnit=metric

And finally to retrieve the icon from the rss feed you should do the following....
 
  weatherReport --cityId="NAM|US|NY" --tempUnit=metric --outputType=icon

The program can also produce a forcast for the current day, or for the following day. 
In order to get a forcast for the current day set the forcast variable to 1 to get the current
conditions set it to 0 and for the next day set the variable to 2.   If the forcast is set to
anything other than 0 then using the "temp" option will still print the full text output.
